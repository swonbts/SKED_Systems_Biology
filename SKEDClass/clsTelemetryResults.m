classdef clsTelemetryResults 
    properties
        Experiment
    end    
    
    methods         
        function obj = clsTelemetryResults(s)
            
        end
    
        function sConn = ConnString(obj)
            %javaaddpath('./ojdbc5.jar') %Use for running jobs on sapelo
            javaaddpath('../Java/ojdbc5.jar'); %Use when running on PC
           sConn = funCredentials(); 
            
        end
        
        function cDataResults = getMonkeysResults(obj)
            sSQL = ['select distinct subject_id from TELEMETRY.TEL_SUMMARY where experiment_id =''' obj.Experiment ''''];
            cDataResults = fetch(obj.ConnString,sSQL); 
        end
        
        function cDataResults = getCDCodeResults(obj)
            sSQL = 'select distinct name from TELEMETRY.TEL_SUMMARY';
            cDataResults = fetch(obj.ConnString,sSQL); 
        end
        
        function cDataResults = getDataResults(obj,sMonkey,sCDCode,sIni,sEnd,sTime) 
            
                sSQL = strcat('SELECT experiment_id, subject_id, subject_name, name, to_char(sum_date, ''',sTime,''') ',...
                    ' range, mean, STDDEV FROM telemetry.tel_summary ',...
                    ' WHERE ',...
                    ' subject_id =  ', sMonkey{1},' ',...
                    ' and NAME = ''', sCDCode,''' and SUM_DATE >= TO_DATE(''',sIni, ''',''',sTime,''') ',...
                    ' and SUM_DATE < TO_DATE(''',sEnd, ''',''',sTime,''') ',...
                    ' and mean is not null ',...
                    ' order by SUM_DATE');
           cDataResults = fetch(obj.ConnString,sSQL); 
        end
           
           
        
%         function cData = countData(obj,sMonkey,sCDCode,sIni,sEnd,amFilterHigh) 
%             sSQL = ['select count(*) from TELEMETRY.DATA where ID_ALIQUOT', ...
%                 ' in (select id_aliquot from TELEMETRY.ALIQUOT where ID_SUBJECT = ', sMonkey,...
%                  'and cd_code = ''', sCDCode,''') and dt_sample > ''',sIni, ''' ', ...
%                  'and dt_sample < ''',sEnd, ''' ', ...
%                  'and am_filter < ''',amFilterHigh,''' '];    
%              
%              cDataResults = fetch(obj.ConnString,sSQL); 
%         end
    end
end

             