classdef clsSKEDBase 

    properties (SetAccess = public)
        Name
        ID
        ExternalID
        Metadata
        DBConnection
    end
    
    methods(Access = public)
        % Record retrieval. There are differences based upon connection
        % type
        function out = getRecordset(o, sSQL)
            oConn = getDBConnection(o);
            switch class(oConn)
                case 'database.ODBCConnection'
                    %out = exec(o.DBConnection,sSQL);
                    out = exec(oConn,sSQL);
                    out = fetch(out); 
                    out = out.Data; 
                otherwise 
                    %out = fetch(o.DBConnection,sSQL);
                    cursor = exec(oConn,sSQL);
                    cursor = fetchmulti(cursor);
                    out = cursor.Data{2};
            end
        end
        
        % Credentials to connect to the database
        function oConn = getDBConnection(o)
            javaaddpath('..\SQL\ojdbc7.jar');            
            oConn = funCredentials();
            %javaaddpath('../SQLServer/sqljdbc4.jar');            
            %oConn = database.ODBCConnection('ModelDB-Local','sa','00#200'); % ,'portnumber',1433
        end
    end    
end