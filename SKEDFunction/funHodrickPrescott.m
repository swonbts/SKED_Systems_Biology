function [s] = funHodrickPrescott(y,lambda)
% This function produces a smooth time series
% params: y - numeric vector, vector to be filtered
% params: lambda - integer, the filtering parameter
    if size(y,1)<size(y,2)
       y=y';
    end
    t=size(y,1);
    a=6*lambda+1;
    b=-4*lambda;
    c=lambda;
    d=[c,b,a];
    d=ones(t,1)*d;
    m=diag(d(:,3))+diag(d(1:t-1,2),1)+diag(d(1:t-1,2),-1);
    m=m+diag(d(1:t-2,1),2)+diag(d(1:t-2,1),-2);
    %
    m(1,1)=1+lambda;       m(1,2)=-2*lambda;
    m(2,1)=-2*lambda;      m(2,2)=5*lambda+1;
    m(t-1,t-1)=5*lambda+1; m(t-1,t)=-2*lambda;
    m(t,t-1)=-2*lambda;    m(t,t)=1+lambda;
    %
    s=m\y; 
    %s=inv(m)*y;
    % hpfilter
end