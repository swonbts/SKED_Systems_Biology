function tExperiment = funGetExperimentSummary(cStructures,sExp)
    
    for i = 1:length(cStructures)
        if strcmp(cStructures{i}.Data{1,1}.ExperimentID,sExp)
            idx = i; 
        end
    end
    
    strucExp = cStructures{idx}; 
    
    cSubject = {};
    cDataType = {}; 
    
    for i = 1:size(strucExp.Data,1)
            cSubject{i} = strucExp.Data{i,1}.Name;

        for j = 1:size(strucExp.Data,2)
            cDataType{i,j} = strucExp.Data{i,j}.DataType;
            cDataProtocol{i,j} = strucExp.Data{i,j}.Metadata.PROTOCOL{1};
        end
    end
    
    
    cUniqueDataTypes = unique(cDataType);
    for i = 1:length(cUniqueDataTypes)
        mUniqueProtocolPerDataType(i) = length(unique(cDataProtocol(strcmp(cDataType,cUniqueDataTypes{i}))));
        for j = 1:length(cSubject)
            mUniqueProtocolPerSubjectPerDataType(j,i) = length(unique(cDataProtocol(j,strcmp(cDataType(j,:),cUniqueDataTypes{i}))));
        end
    end
    
    mTable = [mUniqueProtocolPerDataType;mUniqueProtocolPerSubjectPerDataType];
    cSubject = ['Summary' cSubject];
    cSubject = cSubject';
    cNames = ['Subjects' cUniqueDataTypes'];
    for i = 1:length(cNames) 
        cNames{i} = strrep(cNames{i},' ','_');
    end
    cTable = num2cell(mTable); 
    cExp = [cSubject cTable];
    tExperiment = cell2table(cExp,...
    'VariableNames',cNames);
end