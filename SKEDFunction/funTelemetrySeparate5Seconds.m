function [idxBeginningOfSecond] = funTelemetrySeparate5Seconds(SKEDTime,SKEDData)
% This function takes in a matrix of 5 seconds of data and a corresponding
% time vector and separates it into 5 separate matrices and returns them
% with the time vector 
% Inputs:
%       - SKEDTime: Time vector with dates in date number format
%       - SKEDData: A matrix of data with columns corresponding to 
%                   data types and rows correspond with time points
TimeVec = datevec(SKEDTime);

idxBeginningOfSecond = [];

% Find the index that corresponds to the beginning of each second
for i = 1:5
    dummy = [];
    dummy = and(TimeVec(:,6)<i,TimeVec(:,6)>=i-1);  
    dummy2 = find(dummy,1,'first');
    idxBeginningOfSecond = [idxBeginningOfSecond; dummy2];
end


end