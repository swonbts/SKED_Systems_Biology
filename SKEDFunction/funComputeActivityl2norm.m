function [Time,Activity] = funComputeActivityl2norm(InputTime,ACCX,ACCY,ACCZ)
% This function computes the l2 norm representing activity using the
% accelerometer readings in the X, Y and Z directions
% Inputs:
%       - ACCX - accelerometer reading for X direction
%       - ACCY - accelerometer reading for Y direction
%       - ACCZ - accelerometer reading for Z direction

Activity = [];

% Find index where there is data for each direction
idxX = find(~isnan,ACCX);
idxY = find(~isnan,ACCY);
idxZ = find(~isnan,ACCZ);
 
idxComposite = and(idxX,and(idxY, idxZ)); 
X = ACCX(idxComposite);
Y = ACCY(idxComposite);
Z = ACCZ(idxComposite);
%Using ACCX, ACCY, and ACCZ calculate l2 norm
Activity = sqrt(X.^2 + Y.^2 + Z.^2); 
Time = InputTime(idxComposite);

end
