function [SKEDTime,SKEDData] = getTelemetryTimeSeriesSKED(sExperimentID,nSubjectID,sDataType,dtMinDate,dtMaxDate,FigNum)
% This function retrieves the raw data time series (for a specified range)
% for a given subject and data type.

   % Inputs:
       % sExperimentID - Experiment for Retrieval
       % nSubjectID - Subject ID (Database ID)
                    % Animal Code - Database ID for E30
                    % RKy15 - 1403
                    % REd16	- 1406

                    % Animal Code - Database ID for E07A and E07B
                    % 12C44  - 1407
                    % 12C53  - 1402
                    % H12C59 - 1404
                    % H12C50 - 1410 E07A only
                    % H12C8  - 1405
                    % 11C166 - 1409
                    % 11C131 - 1408
                    % 12C136 - 1401 E07B only

                    % Animal Code - Database ID for E06
                    % RCl15 - 1916
                    % RIh16 - 1912
                    % RTe16 - 1915
                    % RUf16 - 1918
                    
      % sDataType - Data types:
                  % 'TEMP' - Temperature
                  % 'ACC' - Activity (l2 norm of ACCX, ACCY, and ACCZ)
                  % 'BP' - Blood Pressure
                  % 'ECG' - Electrocardiogram
                  % --Note: the data types must be in all caps in order to
                  %         work correctly with clsTelemetrySKED
                  
      % dtMinDate - The lower timestamp boundary (inclusive) specified in DD-MON-YY HH:MI:SS.ff3 AM/PM format (eg. 01-AUG-16 12:00:00.000 AM). 
      % dtMaxDate -  The upper timestamp boundary (exclusive) specified in DD-MON-YY HH:MI:SS.ff3 AM/PM format (eg. 01-SEP-16 03:00:00.000 PM). 

      % lambda - smoothing parameter to be used for Hodrick Prescott filter
    
      % FigNum - Figure Number for data to be plotted on
      
  % Outputs: 
      % SKEDTime - Time vector corresponding to the data
            %    -- Note: The dates returned will be a string specified in 
      %                   YY-MM-DD HH24:MI:SS.ff3 format (eg. '16-12-01 17:30:44.000') 
      % SKEDData - Data vector for specified Data Type
     
%Example: getTelemetryTimeSeriesSKED('E30',1403,'TEMP','01-AUG-16 01:00:00.000 AM','01-AUG-16 02:00:00.000 AM',1)
%         getTelemetryTimeSeriesSKED('E30',1403,'ECG','01-AUG-16 01:00:00.000 AM','01-AUG-16 01:00:01.000 AM',1)  
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
addpath('../../function')  

oTelemetry = clsTelemetrySKED; % Define object

% Retrieve Time Series for specified data type, date range, and subject
cDataSKED = getSKEDResults(oTelemetry,sExperimentID,nSubjectID,sDataType,dtMinDate,dtMaxDate);   
SKEDTime = datenum(cDataSKED(:,1));
SKEDData = cell2mat(cDataSKED(:,2));
    
% Save data in variable with data type name
% and plot the data
switch sDataType
    case 'TEMP'
        TemperatureTime = SKEDTime;
        TemperatureData = SKEDData;
        HPTemperatureData = funHodrickPrescott(TemperatureData, lambda);
%         figure(FigNum);
%         plot(TemperatureTime,TemperatureData,'b'); hold on;
%         plot(TemperatureTime,HPTemperatureData,'r'); hold on;
%         datetick('x',21);
%         xlabel('Time');
%         ylabel('Temperature (in Celsius)');
%         title(['Temperature for Subject: ' num2str(nSubjectID) ' in ' num2str(sExperimentID) '']);
        
    case 'ACC'
        ActivityTime = SKEDTime;
        ActivityData = SKEDData;
        HPActivityData = funHodrickPrescott(ActivityData, lambda);
        figure(FigNum);
        plot(ActivityTime,ActivityData,'b'); hold on;
        plot(ActivityTime,HPActivityData,'r'); hold on;
        xlabel('Time');
        ylabel('Activity');
        title(['Activity for Subject: ' num2str(nSubjectID) ' in ' num2str(sExperimentID) '']);
        
    case 'BP'
        %Subtract the APR READING averaged per minute
        cDataSKEDAPR = getSKEDResults(oTelemetry,sExperimentID,nSubjectID,'APR',dtMinDate,dtMaxDate); 
        APRData = cell2mat(cDataSKEDAPR(:,2));
        BloodPressureTime = SKEDTime;
        BloodPressureData = SKEDData-APRData;

        HPActivityData = funHodrickPrescott(ActivityData, lambda);
        figure(FigNum);
        plot(BloodPressureTime,BloodPressureData,'b'); hold on;
        plot(BloodPressureTime,HPBloodPressureData,'r'); hold on;
        xlabel('Time');
        ylabel('BloodPressure');
        title(['Blood Pressure for Subject: ' num2str(nSubjectID) ' in ' num2str(sExperimentID) '']);
        
    case 'ECG'
        ECGTime = SKEDTime;
        ECGData = SKEDData;
%         MeanECGData = mean(SKEDData);
%         timeperiod = ECGTime;
%         windowsize = size(ECGTime);
%         timeperiod = windowsize(1);
%         CMAECGData = tsmovavg(ECGData,'e',timeperiod);
        %RpeakHeight = findpeaks(ECGData,'Threshold',MeanECGData)
        %HPECGData = funHodrickPrescott(ECGData, lambda);
        %[locs_Rwave] = findpeaks(ECGData,'MinPeakHeight',0.5);
        %NormalizedData = (ECGData - MeanECGData)/(std(ECGData));
%         figure(FigNum);
%         plot(ECGTime,ECGData,'b*'); hold on;
%         %plot(locs_Rwave,ECG(locs_Rwave),'r'); hold on;
%         plot(ECGTime,CMAECGData,'r'); hold on;
%         %plot(ECGTime,NormalizedData,'r'); hold on;
%         datetick('x',14);
%         xlabel('Time');
%         ylabel('ECG');
%         title(['ECG for Subject: ' num2str(nSubjectID) ' in ' num2str(sExperimentID) ', beginning ' dtMinDate '']);
% end
end
