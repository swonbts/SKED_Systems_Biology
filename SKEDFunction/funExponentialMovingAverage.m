function q = funExponentialMovingAverage(y,a)
%Calculates the Exponential Moving Average  of y with a parameter a
%   Detailed explanation goes here
    q = zeros(size(y)); 
    for i=1:max(size(y))
        if i==1
            q(i) = y(i);
        else
            q(i) = a*y(i)+(1-a)*q(i-1);
        end
    end
end

