function [Higherorder2,Higherorder3,Higherorder,Writhe,Avgcross]=funComputeGeometricFeaturesSecond(ACC,ECG,BP,nPoints,nDownsamplePts)
    p1=randperm(length(ECG),900); %randomly selects 900 points
    p2=randperm(length(BP),nPoints);  %randomly selects number of points based on experiment
    x=ACC(1:8);
    y1=ECG(p1);
    z=BP(p2);
    xrange=(1:8);
    xqrange=linspace(1,8,50); % gives 50 evenly spaced point between the min and max number of points for ACC
    LININTACC=interp1(xrange,x,xqrange); %linear interpolation using xqrange to get 50 points 
    dsECG=downsample(y1,18)'; %selects every 18th point of ECG
    dsBP=downsample(z,nDownsamplePts)'; 
    X1=[LININTACC;dsECG;dsBP]';
    I12=0;
    I12A=0;

    for m=1:length(X1)-2
        for n=m+1:length(X1)-1
            y=ComputeWrithe(X1(m,:),X1(m+1,:),X1(n,:),X1(n+1,:));
            z=abs(y);
            mWrith(m,n) = y; 
        end
        I12=I12+y;
        I12A=I12A+z;
    end   
    I1234 = 0;
    for m=1:length(X1)-4
        for n=m+1:length(X1)-3
            temp1 = mWrith(m,n);
            for q=n+1:length(X1)-2
                for s=q+1:length(X1)-1                            
                    temp1=temp1*mWrith(q,s);
                    m1234(q,s)=temp1;
                    I1234=I1234+temp1;
                end     
            end
        end 
    end
    I1256=0;
    for k=1:length(X1)-6
        for j=k+1:length(X1)-5
            temp1 = mWrith(k,j);
            for q=j+2:length(X1)-2
                for s=q+1:length(X1)-1                            
                    temp3=temp1*mWrith(q,s);
                    I1256=I1256+temp3;
                end     
            end
        end 
    end
    I123456=0;
    for k=1:length(X1)-6
        for j=k+1:length(X1)-5
            temp1 = mWrith(k,j);
            for m=j+1:length(X1)-4
                for n=m+1:length(X1)-3
                    temp2 = mWrith(m,n);
                    for q=n+1:length(X1)-2
                        for s=q+1:length(X1)-1                            
                            temp3=temp1*temp2*mWrith(q,s);
                            I123456=I123456+temp3;
                        end     
                    end
                end 
            end
        end
    end
    %Gives writhe, higherorder, and average crossing number
    Higherorder2=I1256/(4*pi^2);
    Higherorder3=I123456/(8*pi^3);
    Higherorder=I1234/(4*pi^2);        
    Writhe = I12/(2*pi);
    Avgcross=I12A/(2*pi);
end
