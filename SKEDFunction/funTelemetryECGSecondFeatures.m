function [AvgRPeakHeight, BPM,ECGDetrended] = funTelemetryECGSecondFeatures(Time, ECGData)
% This function receives one second of raw EGG data and does the following:
%   1 - Find R peaks
%   2 - Finds the average R peak height
%   3 - Calculates Beats per minute

%   Inputs:
           % Time - time vector corresponding to ECG Data
           % ECGData - vector of raw ECG data for one second
%   Outputs: 
           % Time - Initial time point
           % AvgRPeakHeight - The average of the R peak heights
           % BPM - Heart beats per minutes estimated using this second of
           % data 


a = 0.5; % Smoothing Parameter for EMA 
ECGEMA = funExponentialMovingAverage(ECGData,a);
locs_Rpeak = [];
RPeakHeightSum = 0;

ECGDetrended = ECGData - ECGEMA;
ECGMean = mean(ECGDetrended);
ECGStandardDeviation = std(ECGDetrended);

MinRPeakHeight = ECGMean + ECGStandardDeviation;
[pks,locs_Rpeak] = findpeaks(ECGDetrended,'MinPeakHeight',MinRPeakHeight); % Find R Peaks

nRPeaks = length(pks);

% Compute Average R Peak Height
for i = 1 : nRPeaks
    RPeakHeightSum = RPeakHeightSum + pks(i,1);
end
AvgRPeakHeight = RPeakHeightSum/nRPeaks;

% Estimate heartbeats per minute based on this second of data
BPM = nRPeaks * 60; 

end


