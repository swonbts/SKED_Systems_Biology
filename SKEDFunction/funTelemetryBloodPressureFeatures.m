function [BPSystolicAvg, BPDiastolicAvg] = funTelemetryBloodPressureFeatures(BPData)
% This function takes in raw blood pressure for one second and finds the
% systolic and diastolic blood pressure

BPMean = mean(BPData);
BPStandardDeviation = std(BPData);
BPThreshold = BPMean + BPStandardDeviation;
BPSmoothed = funHodrickPrescott(BPData,100);
[Systolic_pks,locs_Systolic] = findpeaks(BPSmoothed,'MinPeakHeight',BPThreshold); 
BPDataInverted = BPSmoothed.*(-1);
BPThresholdNegative = BPThreshold*(-1);
[Diastolic_pks,locs_Diastolic] = findpeaks(BPDataInverted,'MinPeakHeight',BPThresholdNegative);
Diastolic_pks = Diastolic_pks.*(-1);
BPDiastolicSize = length(Diastolic_pks);
BPSystolicSize = length(Systolic_pks);
BPSystolicAvg = sum(Systolic_pks)/BPSystolicSize;
BPDiastolicAvg = sum(Diastolic_pks)/BPDiastolicSize;
end
