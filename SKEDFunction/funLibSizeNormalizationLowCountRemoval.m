function strucData = funLibSizeNormalizationLowCountRemoval(strucData,nCount)

    nIdentifier = []; 
    mCountData =[];
    for i = 1:length(strucData.cRawCounts)
        mCountData = [mCountData strucData.cRawCounts{i}];
        tempIdentifier = [];
        tempIdentifier(1:size(strucData.cRawCounts{i},2),1) = i;
        nIdentifier = [nIdentifier;tempIdentifier];
    end
    
    pseudoRefSample = geomean(mCountData,2);
    nz = pseudoRefSample > 0;
    ratios = bsxfun(@rdivide,mCountData(nz,:),pseudoRefSample(nz));
    sizeFactors = median(ratios,1);

    normCounts = bsxfun(@rdivide,mCountData,sizeFactors);
    
    figure('units','normalized','outerposition',[0 0 1 1]);

    subplot(2,1,1)
    maboxplot(log2(mCountData),'title','Raw Read Count','orientation','horizontal')
    ylabel('sample')
    xlabel('log2(counts)')

    subplot(2,1,2)
    maboxplot(log2(normCounts),'title','Normalized Read Count','orientation','horizontal')
    ylabel('sample')
    xlabel('log2(counts)')
    
    lowCountThreshold = nCount;
    lowCountGenes = all(normCounts < lowCountThreshold, 2);
    
    normCounts = normCounts(~lowCountGenes,:);
    for i = 1:length(strucData.cRawCounts)

        strucData.cRawCounts{i} = normCounts(:,nIdentifier == i);
        strucData.VarNames{i} = strucData.VarNames{i}(~lowCountGenes);
    end
end