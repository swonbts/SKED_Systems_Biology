%Animal Code - Database ID for E30
% RKy15 1403
% REd16	1406

% Animal Code - Database ID for E07
% 12C44  1407
% 12C53  1402
% H12C59 1404
% H12C50 1410
% H12C8  1405
% 11C166 1409
% 11C131 1408
% 12C136 1401

% Animal Code - Database ID for E06
% RCl15 1916
% RIh16 1912
% RTe16 1915
% RUf16 1918
function [sLegend, Y, I, mRawDaily] = funWritheDaily(sExperiment, sMonkeyName)
    %sMonkeyName = '1406'; sExperiment = 'E30';
    nHP = 1e3; 
    bHoldOn = true; 
    load([sExperiment '_Telemetry_Summary_Results_' sMonkeyName '.mat'])
    k = 0;
    I12=0;
    I12A=0;
    sFile = ['DailyWrite_' sExperiment '_' sMonkeyName '.mat']; 
    if exist(sFile)
        load(sFile)
        % Find common times for all variables
        [ACCMean,ACCSD,ACCTime,...
            BloodPressureMean,BloodPressureSD,BloodPressureTime,...
            ECGMean,ECGSD,ECGTime,...
            TemperatureMean, TemperatureSD, TemperatureTime] = ...
        funCorrectDates(ACCMean,ACCSD,ACCTime,...
            BloodPressureMean,BloodPressureSD,BloodPressureTime,...
            ECGMean,ECGSD,ECGTime,...
            TemperatureMean, TemperatureSD, TemperatureTime);
    else
        tic
        Higherorder = []; Writhe = []; AvgCrossNumbr = []; mRawDaily = []; fDailyDate = [];
        % Find common times for all variables
        [ACCMean,ACCSD,ACCTime,...
            BloodPressureMean,BloodPressureSD,BloodPressureTime,...
            ECGMean,ECGSD,ECGTime,...
            TemperatureMean, TemperatureSD, TemperatureTime] = ...
        funCorrectDates(ACCMean,ACCSD,ACCTime,...
            BloodPressureMean,BloodPressureSD,BloodPressureTime,...
            ECGMean,ECGSD,ECGTime,...
            TemperatureMean, TemperatureSD, TemperatureTime);
        % Pick a time vector, and move on!
        TimeVec = datevec(BloodPressureTime);
        uMonth = unique(TimeVec(:,2));
        if 1==1
            for i=1:size(uMonth,1)
                disp(['Month ' num2str(uMonth(i))]);
                idx = TimeVec(:,2) == uMonth(i);  
                dummy1 = unique(TimeVec(idx,3));
                for j = min(dummy1):max(dummy1)
                    disp(['Day ' num2str(j)]);
                    I12=0;
                    I12A=0;
                    idxTime = and(TimeVec(:,2)==uMonth(i),TimeVec(:,3)==j);
                    X1=[];
                    k = k+ 1;
    %                 dummy = ACCSD(idxTime);
                    dummy2 = ACCMean(idxTime); 
                    %idx = dummy < 4080; dummy(idx) = 4080; %CHECK THIS
                    X1(:,1) = dummy2; 
    %                 X1(:,2) = BloodPressureSD(idxTime); 
    %                 X1(:,3) = TemperatureSD(idxTime); 
    %                 dummy2 = ECGSD(1:size(idxTime,1));
                    X1(:,2) = BloodPressureMean(idxTime); 
                    X1(:,3) = TemperatureMean(idxTime); 
                    dummy2 = ECGMean(1:size(idxTime,1));
                    %X1(:,4) = dummy2(idxTime); 


                    mRawDaily(k,:) = [ mean(X1(:,1)), ... 
                                        mean(X1(:,2)), ...
                                        mean(X1(:,3)), ...
                                        mean(dummy2) ]; 
                    fDailyDate(k) = datenum(2016, uMonth(i), j);
                    if 1==1
                        if length(X1)>3
                            for m=1:length(X1)-2
                                disp(['Month ' num2str(uMonth(i)) ' Day ' num2str(j) ' I12 ' num2str(m) ' out of ' num2str(length(X1)-2)])
                                for n=m+1:length(X1)-1
                                    y=Computewrithe(X1(m,:),X1(m+1,:),X1(n,:),X1(n+1,:));
                                    z=abs(y);
                                    mWrith(m,n) = y; 
                                end
                                I12=I12+y;
                                I12A=I12A+z; 
                            end   
                        end
                    end
                    I1234 = 0;
                    if 1==2 %length(X1)>3
                        for m=1:length(X1)-4
                            for n=m+1:length(X1)-3
                                temp1 = mWrith(m,n);
                                for q=n+1:length(X1)-2
                                    for s=q+1:length(X1)-1                            
                                        temp1=temp1*mWrith(q,s);
                                        I1234=I1234+temp1;
                                    end     
                                end
                            end 
                        end
                    end
                    Higherorder(k)=I1234/(4*pi^2);        
                    Writhe(k) = I12/(2*pi);
                    AvgCrossNumbr(k)=I12A/(2*pi);

                    I12=0;
                    I12A=0;
                end
            end
            toc

            save(sFile, 'Higherorder', 'Writhe', 'AvgCrossNumbr', 'mRawDaily', 'fDailyDate'); 
        end
    end


    % Compute cummulative Writhe
    CumWrithe=[];
    for i=1:size(Writhe,2)
        CumWrithe(i)=sum(Writhe(1:i));
    end

    mRawDaily(:,5) = Writhe' ; 
    mRawDaily(:,6) = AvgCrossNumbr' ; 
    mRawDaily(:,7) = Higherorder' ; 
    mRawDaily(:,8) = CumWrithe' ; 

    Y = [];
    idx = isnan(mRawDaily(:,1)); 
    for i = 2:8
        y = mRawDaily(:,i);        
        dummyIdx = isnan(y); 
        idx = or(idx, dummyIdx); 
    end
    for i = 1:8
        y = mRawDaily(:,i);        
        %idx = isnan(y); 
        y(idx) = []; 
        y = funHodrickPrescott(y,nHP);
        Y(:,i) = y; 
    end
    x = fDailyDate(~idx)';


    %y = CumWrithe(idx); %mRawDaily(:,1); 
    %Definition of time points
        switch sExperiment
        case 'E30'
            dtTP(1) = datenum('7/28/2016');
            dtTP(2) = datenum('7/31/2016');
            dtTP(3) = datenum('8/12/2016');
            dtTP(4) = datenum('8/15/2016');% Inoculation. TP numbers after this are increased by 1
            dtTP(5) = datenum('8/18/2016');% Parasitemia
            dtTP(6) = datenum('8/21/2016');
            dtInnoculation = datenum('8/11/2016');
            sMarker = 'o';
        case 'E07A'
            dtTP(1) = datenum('10/21/2016'); %Day after Implant is turned on
            dtTP(2) = datenum('10/25/2016'); 
            dtTP(3) = datenum('11/02/2016');
            dtTP(4) = datenum('11/05/2016'); % Inoculation. TP numbers after this are increased by 1
            dtTP(5) = datenum('11/12/2016'); % Parasitemia should have appeared here
            dtTP(6) = datenum('11/14/2016'); % Day before implants were turned off
            dtInnoculation = datenum('11/01/2016');
            sMarker = 's';
        case 'E07B'
            dtTP(1) = datenum('1/11/2016'); 
            dtTP(2) = datenum('1/15/2016');
            dtTP(3) = datenum('1/21/2016');
            dtTP(4) = datenum('1/25/2016'); 
            dtTP(5) = datenum('1/28/2016'); 
            dtTP(6) = datenum('1/30/2016'); 
            dtInnoculation = datenum('1/20/2016');
            sMarker = '^';
        case 'E06'
            dtTP(1) = datenum('3/23/2016');
            dtTP(2) = datenum('3/25/2016');
            dtTP(3) = datenum('4/03/2016');
            dtTP(4) = datenum('4/08/2016'); 
            dtTP(5) = datenum('4/10/2016'); 
            dtTP(6) = datenum('4/13/2016'); 
            dtInnoculation = datenum('4/03/2016');
            sMarker = 'h';
    end  
    % % Assign the working variable to fData, so that we can use the same
    % % visualization with many variables
    % %h=figure(1);
    % plot(x,y)
    % % Plot time points
    % for i=2:size(dtTP,2) 
    %     pl = line([dtTP(i), dtTP(i)], [min(y),max(y)])
    %     pl.Color = 'green';
    %     pl.LineStyle = '--';
    % end
    % % Plot Innoculation
    % pl = line([dtInnoculation, dtInnoculation], [min(y),max(y)])
    % pl.Color = 'red';
    % pl.LineStyle = '--';
    % % Show date in cursor3
    % dcm_obj = datacursormode(h);
    % set(dcm_obj,'UpdateFcn',@funDateCursor)
    % dateFormat = 25;
    % datetick('x',dateFormat)
    % %NumTicks = 10;
    % %L = get(gca,'XLim');
    % %set(gca,'XTick',linspace(L(1),L(2),NumTicks))
    % xtickangle(45)
    % %axis tight


    h = figure(2);
    if bHoldOn
        hold on;
    else
        clf; 
    end
    % Show date in cursor3
    %dcm_obj = datacursormode(h);
    %set(dcm_obj,'UpdateFcn',@funDateCursor3D)
    % Select variables
    %1 = Activity
    %2 = Blood pressure
    %3 = Temperature
    %4 = ECG
    %5 = Writhe
    %6 = AvgCrossNumbr
    %7 = Higherorder
    %8 = CumWrithe
    i = 1; j = 2; k = 3;
    nMarkerSize = 50; 
    
    
    % Pre-infection
    idx = and(x >= dtTP(1), x <= dtTP(2)); %x <=datenum('8/11/2016'); 
    I = zeros(size(idx,1), 1); 
    scatter3(Y(idx,i), Y(idx,j), Y(idx,k),nMarkerSize,'Marker',sMarker,'MarkerFaceColor','blue'); 
    I(idx) = 1; 
    %legend('Pre-Infection'); 
    hold on;
    
    % Liver stage
    idx = and(x >= dtTP(3), x <= dtTP(4)); 
    scatter3(Y(idx,i), Y(idx,j), Y(idx,k), nMarkerSize, 'Marker',sMarker,'MarkerFaceColor','green'); 
    I(idx) = 2; 
    %legend('Liver'); 
    hold on;
    
    % Blood stage
    idx = and(x >= dtTP(5), x <= dtTP(6));  
    scatter3(Y(idx,i), Y(idx,j), Y(idx,k),nMarkerSize,'Marker',sMarker,'MarkerFaceColor','red'); 
    I(idx) = 3; 
    % Build legend
    sLegend = {[sExperiment '-' sMonkeyName ' Pre'],... 
                [sExperiment '-' sMonkeyName ' Liver'], ...
                [sExperiment '-' sMonkeyName ' Blood']}; 
    axis tight

end


function [ACCMean,ACCSD,ACCTime,...
            BloodPressureMean,BloodPressureSD,BloodPressureTime,...
            ECGMean,ECGSD,ECGTime,...
            TemperatureMean, TemperatureSD, TemperatureTime] = ...
        funCorrectDates(ACCMean,ACCSD,ACCTime,...
            BloodPressureMean,BloodPressureSD,BloodPressureTime,...
            ECGMean,ECGSD,ECGTime,...
            TemperatureMean, TemperatureSD, TemperatureTime)

    % First: Find common times
    dummy0 = intersect(ACCTime, intersect(BloodPressureTime, intersect(ECGTime, TemperatureTime)));
    % Accelerometers
    idx = ismember(ACCTime,dummy0);
    ACCMean = ACCMean(idx); 
    idxACC = isnan(ACCMean); 
    ACCSD = ACCSD(idx); 
    ACCTime = ACCTime(idx); 
    % BloodPressure
    idx = ismember(BloodPressureTime,dummy0);
    BloodPressureMean = BloodPressureMean(idx); 
    idxBloodPressure = isnan(BloodPressureMean); 
    BloodPressureSD = BloodPressureSD(idx); 
    BloodPressureTime = BloodPressureTime(idx); 
    % ECG
    idx = ismember(ECGTime,dummy0);
    ECGMean = ECGMean(idx); 
    idxECG = isnan(ECGMean); 
    ECGSD = ECGSD(idx); 
    ECGTime = ECGTime(idx); 
    % Temperature
    idx = ismember(TemperatureTime,dummy0);
    TemperatureMean = TemperatureMean(idx); 
    idxTemperature = isnan(TemperatureMean); 
    TemperatureSD = TemperatureSD(idx); 
    TemperatureTime = TemperatureTime(idx); 

    % Scond: Remove all NaNs and normalize
    idx = ~or(idxACC,or(idxBloodPressure,or(idxECG,idxTemperature)));
    % Accelerometers
    ACCMean = ( ACCMean(idx) - mean(ACCMean(idx)) ) / std(ACCMean(idx));     
    ACCSD = ACCSD(idx); 
    ACCTime = ACCTime(idx); 
    % BloodPressure
    BloodPressureMean = ( BloodPressureMean(idx) - mean(BloodPressureMean(idx)) ) / std(BloodPressureMean(idx))  ; 
    BloodPressureSD = BloodPressureSD(idx); 
    BloodPressureTime = BloodPressureTime(idx); 
    % ECG
    ECGMean = ( ECGMean(idx) - mean(ECGMean(idx)) ) / std(ECGMean(idx)); 
    ECGSD = ECGSD(idx); 
    ECGTime = ECGTime(idx); 
    % Temperature
    TemperatureMean = ( TemperatureMean(idx) - mean(TemperatureMean(idx)) ) /  std(TemperatureMean(idx)); 
    TemperatureSD = TemperatureSD(idx); 
    TemperatureTime = TemperatureTime(idx); 
end