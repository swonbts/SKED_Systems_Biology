function out = funFormatTimeTick(t,n)
    % This function formats the time axis for any plot
    % Input:    t = date vector in string format
    %           n = gap in date ticks
    T = datenum(t);
    t = datestr(T,'mm/dd/yy');  
    idx2 = 1:n:length(t);
    ax = gca;
    set(ax,'XTick',T(1):n:T(end));
    set(ax,'XTickLabel',t(idx2,:));
    dateFormat = 1; 
    xtickangle(90); 
    % Set axis tight only on y-axes
    yl=ylim(ax); % retrieve auto y-limits
    axis tight   % set tight range
    ylim(ax,yl)  % restore y limits 
    grid on; 
end