function strucData = funGetProtocolData(cStructures,sExp,sSubjects,sProtocol)  

        for i = 1:length(cStructures)
            if strcmp(cStructures{i}.Data{1,1}.ExperimentID,sExp)
                idx = i; 
            end
        end

        strucExp = cStructures{idx}; 

        cSubject = {};

        for i = 1:size(strucExp.Data,1)
                cSubject{i} = strucExp.Data{i,1}.Name;
        end
        
         if strcmp(sSubjects,'All') == 0
            [temp ia ib] = intersect(cSubject,sSubjects);

            for i = 1:length(ia)
                    [x,t,s] = strucExp.retrieveProtocolData(ia(i),sProtocol); 
                    strucData.cRawCounts{i} = x; 
                    strucData.cTime{i} = t;
                    strucData.cSubject{i} = cSubject(ia(i));
                    strucData.VarNames{i} = s;
            end





         
        elseif strcmp(sSubjects,'All')
            for i = 1:length(cSubject)
                 [x,t,s] = strucExp.retrieveProtocolData(i,sProtocol); 
                  strucData.cRawCounts{i} = x; 
                  strucData.cTime{i} = t;
                  strucData.cSubject{i} = cSubject(i);
                  strucData.VarNames{i} = s;

            end

         end


end