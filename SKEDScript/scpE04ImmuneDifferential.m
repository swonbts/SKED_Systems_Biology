addpath('../function');
sExpName = 'E04'; %%%Experiment of Interest is E04 
sFileName = [sExpName 'CompleteData.mat'];%%%%Define Save Destination and File Name 
cDataTypes = {'Clinical','Immunology','Functional Genomics','Lipidomics'}; 
    load(sFileName) 
mIdxImmune = oExplorer.findDataType('Immunology');  %%%%find the index of Immunology data within oExplorer
mIdxClinical = oExplorer.findDataType('Clinical');  %%%%find the index of Clinical data within oExplorer

oExplorerAggregated = MDBAnalysis(); 

for i = 1:4 
    %%%aggregateTimeSeries is used to align different type series by
    %%%collection type and keep the intersection of collection dates. 
    %%%For example, clinical is collected on day 1 to 100, and FXGN whole
    %%%blood is collected on day 1 2 5 8 19. Then the aggregated time
    %%%series are from day 1 2 5 8 19. 
    
    oExplorerAggregated.Data{i} = oExplorer.aggregateTimeSeries(i,[mIdxClinical(i,:) mIdxImmune(i,:)]);
end

%%%Filter out time series that contains missing data. 

oExplorerAggregated = oExplorerAggregated.filterMissingData(); 
%%%Filter out time series that do not appear for all the subjects. 

oExplorerAggregated = oExplorerAggregated.filterSharedVariables();

cVarName = oExplorerAggregated.Data{1}.DataPrimitive.VarNames; 

pVal = []; 
mOverSevere = [];
mOverMild = []; 
for i = 1:length(cVarName)
    
    for j = 1:size(oExplorerAggregated.Data{1}.DataPrimitive.Table,2)
        Expression = []; 
            for k = 1:4 
                idx = find(strcmp(oExplorerAggregated.Data{k}.DataPrimitive.VarNames,cVarName{i}));
                Expression(k) = oExplorerAggregated.Data{k}.DataPrimitive.Table(idx,j);
            end
        [~, tpVal] = ttest2(Expression(1:2),Expression(3:4));
        pVal(i,j) = tpVal; 
        mOverSevere(i,j) = mean(Expression(1:2))/mean(Expression(3:4)); 
    end

end 

mOverMild = 1./mOverSevere; 


    fid = fopen(['ClinImmuneTTest.txt'],'w'); 
    fprintf(fid,'Name\tTimePoint\tSevereOverExpression\tpVal\n');
        for i = 1:7
            tempName = cVarName(pVal(:,i)<=0.05);
            tempFC = mOverSevere(pVal(:,i)<=0.05,i);
            tempPval = pVal(pVal(:,i)<=0.05,i);
            for j = 1:length(tempName)
                temp = [tempName{j} '\t' ,...
                        ['TP' num2str(i)] '\t',...
                        num2str(tempFC(j)) '\t',...
                        num2str(tempPval(j)) '\n'];
                fprintf(fid,temp);
            end
        end 
    fclose(fid); 


pValSevere = []; 
ChangeSevere = [];
pValMild = [];
ChangeMild = [];
for i = 1:length(cVarName)
    
    for j = 1:size(oExplorerAggregated.Data{1}.DataPrimitive.Table,2)-1
        Expression = [];  
        idx = find(strcmp(oExplorerAggregated.Data{1}.DataPrimitive.VarNames,cVarName{i}));
        
        Expression(1) = oExplorerAggregated.Data{1}.DataPrimitive.Table(idx,j);
        Expression(2) = oExplorerAggregated.Data{2}.DataPrimitive.Table(idx,j);
        Expression(3) = oExplorerAggregated.Data{1}.DataPrimitive.Table(idx,j+1);
        Expression(4) = oExplorerAggregated.Data{2}.DataPrimitive.Table(idx,j+1);
        [~, tpVal] = ttest2(Expression(1:2),Expression(3:4));
        pValSevere(i,j) = tpVal; 
        ChangeSevere(i,j) = mean(Expression(3:4))/mean(Expression(1:2)); 
        
        Expression = [];  
        
        Expression(1) = oExplorerAggregated.Data{3}.DataPrimitive.Table(idx,j);
        Expression(2) = oExplorerAggregated.Data{4}.DataPrimitive.Table(idx,j);
        Expression(3) = oExplorerAggregated.Data{3}.DataPrimitive.Table(idx,j+1);
        Expression(4) = oExplorerAggregated.Data{4}.DataPrimitive.Table(idx,j+1);
        [~, tpVal] = ttest2(Expression(1:2),Expression(3:4));
        pValMild(i,j) = tpVal; 
        ChangeMild(i,j) = mean(Expression(3:4))/mean(Expression(1:2)); 
    end

end 

fid = fopen(['ClinImmuneDifferentialRegulation.txt'],'w'); 
    fprintf(fid,'Name\tSeverity\tChange\tpVal\n');

            tempName = cVarName(pValSevere(:,1)<=0.05);
            tempFC = ChangeSevere(pValSevere(:,1)<=0.05,1);
            tempPval = pValSevere(pValSevere(:,1)<=0.05,1);
            for j = 1:length(tempName)
                temp = [tempName{j} '\t' ,...
                        ['Severe'] '\t',...
                        num2str(tempFC(j)) '\t',...
                        num2str(tempPval(j)) '\n'];
                fprintf(fid,temp);
            end
            
            tempName = cVarName(pValMild(:,1)<=0.05);
            tempFC = ChangeMild(pValMild(:,1)<=0.05,1);
            tempPval = pValMild(pValMild(:,1)<=0.05,1);
            for j = 1:length(tempName)
                temp = [tempName{j} '\t' ,...
                        ['Mild'] '\t',...
                        num2str(tempFC(j)) '\t',...
                        num2str(tempPval(j)) '\n'];
                fprintf(fid,temp);
            end
    fclose(fid); 