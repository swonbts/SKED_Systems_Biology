clear
addpath('../function');
sExpName = 'E04'; %%%Experiment of Interest is E04 

sSevere = {'RMe','RFa'};
sMild = {'RIc','RSb'};
sFileName = [sExpName 'CompleteData.mat'];%%%%Define Save Destination and File Name 
cDataTypes = {'Clinical','Immunology','Functional Genomics','Lipidomics'}; 
    load(sFileName) 
mIdxImmune = oExplorer.findDataType('Immunology');  %%%%find the index of Immunology data within oExplorer
mIdxClinical = oExplorer.findDataType('Clinical');  %%%%find the index of Clinical data within oExplorer
%oExplorer = setMeanVarTimeSeries(oExplorer,[]); 

oExplorerAggregated = MDBAnalysis(); 

for i = 1:4 
    %%%aggregateTimeSeries is used to align different type series by
    %%%collection type and keep the intersection of collection dates. 
    %%%For example, clinical is collected on day 1 to 100, and FXGN whole
    %%%blood is collected on day 1 2 5 8 19. Then the aggregated time
    %%%series are from day 1 2 5 8 19. 
    
    oExplorerAggregated.Data{i} = oExplorer.aggregateTimeSeries(i,[mIdxClinical(i,:) mIdxImmune(i,:)]);
end

%%%Filter out time series that contains missing data. 

oExplorerAggregated = oExplorerAggregated.filterMissingData(); 

%%%Filter out time series that do not appear for all the subjects. 

oExplorerAggregated = oExplorerAggregated.filterSharedVariables();

cLabels = {'RMe','RFa','RIc','RSb'};

mData = []; 
for i = 1:4 
    mData = [mData oExplorerAggregated.Data{i}.DataPrimitive.Table];
end 

mPdist = pdist(mData,'spearman');
mSquareDist = squareform(mPdist); 
mSquareDist(mSquareDist == 0) = 1;

[num text raw] = xlsread('cell_cytokine.xlsx');

cNames = {}; 
for k = 1:2
    temp = {};
    for j = 1:size(text,1)
        if isempty(text{j,k}) == 0
            temp = vertcat(temp,text{j,k});
        end
    end
    cNames{k} = temp;
end
[RHO,PVAL] = corr(mData','type','spearman');
temp = PVAL; 
n = size(temp,1);
temp(1:n+1:n*n) = 0;
temp = squareform(temp);
temp = mafdr(temp,'BHFDR',1);
PVAL = squareform(temp); 
PVAL(PVAL == 0) = 1; 

for k = 1:2 
    for j = 1:length(cNames{k})
        temp = cNames{k}{j}; 
        
        idx = find(strcmp(temp,oExplorerAggregated.Data{1}.DataPrimitive.VarNames));
        if isempty(idx) == 0
            IL6Corr = mSquareDist(idx,:);
            IL6Pval = PVAL(idx,:);
            [a b] = sort(IL6Corr,'ascend');

            for i = 1:20
                if IL6Pval(b(i)) <= 0.05 & 1-IL6Corr(b(i)) >= 0.8
                    h = figure('units','normalized','outerposition',[0 0 1 1]);
                    plot(log(mData(idx,1:7)),log(mData(b(i),1:7)),'ro','MarkerSize',10,'LineWidth',2);
                    hold on 
                    plot(log(mData(idx,8:14)),log(mData(b(i),8:14)),'rx','MarkerSize',10,'LineWidth',2);
                    plot(log(mData(idx,15:21)),log(mData(b(i),15:21)),'bo','MarkerSize',10,'LineWidth',2);
                    plot(log(mData(idx,22:end)),log(mData(b(i),22:end)),'bx','MarkerSize',10,'LineWidth',2);

                    legend(cLabels)
                    xlabel(temp);
                    ylabel(oExplorerAggregated.Data{1}.DataPrimitive.VarNames(b(i)));
                    title(['corr = ' num2str((1 - IL6Corr(b(i)))) ' pval = ' num2str(IL6Pval(b(i)))])
                    set(gca,'FontSize',18);
                    sFileName = ['Correlation/' temp '_' oExplorerAggregated.Data{1}.DataPrimitive.VarNames{b(i)}];

                    funPrintImage(h,sFileName);
                    close all
                end

            end
        end
    end
end