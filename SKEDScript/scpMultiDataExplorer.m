% Call the script that loads data in RAM
if ~(exist('cStructures','var'))
    scpMultiDataLoad;
end
% Display summary of data collected per experiment
clc
for k=1:length(cStructures)
    oSKEDAnalysis = cStructures{k}; 
    [nSubject, nProtocol] = size(oSKEDAnalysis.Data); 
    for i = 1:nSubject
        disp('********************************')
        for j = 1:nProtocol
            % Display Experiment ID and subject ID
            disp([oSKEDAnalysis.Data{i,j}.ExperimentID ': ' oSKEDAnalysis.Data{i,j}.Name]);
            % Display name of data types and size
            disp([oSKEDAnalysis.Data{i,j}.DataType ': ' num2str(size(oSKEDAnalysis.Data{i,j}.DataPrimitive.VarNames,2))]);
            % Display Sumamry form metadata
            disp(oSKEDAnalysis.Data{i,j}.Metadata.SUMMARY);
            % Display protocol ID and name 
            disp(['Protocol ' num2str(oSKEDAnalysis.Data{i,j}.Metadata.PROTOCOL_APP_ID) ': ' oSKEDAnalysis.Data{i,j}.Metadata.PROTOCOL]);
            disp('---------------------------------')
        end

        % We observed the following protocols names of interest:
        % ME_CBC
        % ME_parasitemia
        % ME_retic
        % Only in E23, E24, E25: ME_istat
        % LI_REL_QUAN
        % WARNING: Search oSKEDAnalysis.Data{i,j}.Metadata.SUMMARY for tissue type
        % Explore visually some variables
        nfig = i+10*k; 
        figure(nfig);clf; 
        title([oSKEDAnalysis.Data{i,j}.ExperimentID ': ' oSKEDAnalysis.Data{i,j}.Name]);
        % Load RBC count
        yyaxis right; 
        [x,t,s] = oSKEDAnalysis.retrieveProtocolData(i, 'ME_CBC'); 
        idxLeft = find(strcmp(s,'x_rbc_')); 
        T = datenum(t);
        plot(T, x(idxLeft,:));
        ylim([0 8e6])
        ylabel('RBC');
        % Load parasitemia
        yyaxis left; 
        [x,t,s] = oSKEDAnalysis.retrieveProtocolData(i, 'ME_parasitemia'); 
        idxLeft = find(strcmp(s,'x_parasites_')); 
        T = datenum(t);
        plot(T, x(idxLeft,:));
        ylim([0 5e5]);
        ylabel('Parasites');   
        
        funFormatTimeTick(t,2); 
        
        if 1==2
        % Now show up and downregulated genes TP1, TP2 all experiments
        % Only in E04, E23: FG_GENE_EXP_RAW2
        % Except E04, E23: FG_GENE_EXP_RAW
        switch oSKEDAnalysis.Data{i,j}.ExperimentID
            case {'E03', 'E04'}
                sProtocol = 'FG_GENE_EXP_RAW2'; 
            otherwise
                sProtocol = 'FG_GENE_EXP_RAW'; 
        end
        [x,t,s] = oSKEDAnalysis.retrieveProtocolData(i, sProtocol);
        d = log(x(:,1)) - log(x(:,2)); 
        figure(nfig+100)
        hist(d,1000);
        end
    end
end




disp('Finished explorer'); toc