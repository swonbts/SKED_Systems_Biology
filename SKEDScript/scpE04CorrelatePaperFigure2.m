clear
addpath('../function');
load('RFv.mat')

load('E04CompleteData.mat'); 

oExplorer.Data = vertcat(temp,oExplorer.Data);
cLabels = {'RFv','RMe','RFa','RIc','RSb'};


sSevere = {'RFv','RMe','RFa'};
sMild = {'RIc','RSb'};

sDate = '2013-11-20 00:00:00.0'; 
for i = 1:4
    Idx2 = find(strcmp(sDate,oExplorer.Data{i,18}.DataPrimitive.Time));
    n = length(oExplorer.Data{i,18}.DataPrimitive.Time); 
    temp = 1:n ~= Idx2; 
    oExplorer.Data{i,18}.DataPrimitive.Time = oExplorer.Data{i,18}.DataPrimitive.Time(temp);
    oExplorer.Data{i,18}.DataPrimitive.Table = oExplorer.Data{i,18}.DataPrimitive.Table(:,temp);
end

oExplorerAggregated = MDBAnalysis(); 

cDataTypes = {'Clinical','Immunology','Functional Genomics','Lipidomics'}; 
mIdxImmune = oExplorer.findDataType('Immunology');  %%%%find the index of Immunology data within oExplorer
mIdxClinical = oExplorer.findDataType('Clinical');  %%%%find the index of Clinical data within oExplorer
oExplorerAggregated = MDBAnalysis(); 

for i = 1:5
    %%%aggregateTimeSeries is used to align different type series by
    %%%collection type and keep the intersection of collection dates. 
    %%%For example, clinical is collected on day 1 to 100, and FXGN whole
    %%%blood is collected on day 1 2 5 8 19. Then the aggregated time
    %%%series are from day 1 2 5 8 19. 
    
    oExplorerAggregated.Data{i} = oExplorer.aggregateTimeSeries(i,[mIdxClinical(i,:) mIdxImmune(i,:),18]);
end

%%%Filter out time series that contains missing data. 

oExplorerAggregated = oExplorerAggregated.filterMissingData(); 

%%%Filter out time series that do not appear for all the subjects. 

oExplorerAggregated = oExplorerAggregated.filterSharedVariables();

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mData = [];
mData2 = []; 
for i = 1:5
    temp = oExplorerAggregated.Data{i}.DataPrimitive.Table;
    mData = [mData temp];
    for j = 1:size(temp,2)
        temp(:,j) = temp(:,j) - temp(:,1);
    end
    mData2 = [mData2 temp(:,2:end)];
end 

[coeff,score,latent,tsquared,explained,mu] = pca(mData');

x1 =[1:6;7:12;13:18;19:24];
g = [ 1 2;3 4];

h = figure('units','normalized','outerposition',[0 0 1 1]);

cLabel = {'ro','bo','rp','bp','rd','bd','rs','bs'};
c = 1;
for i = 1:3
    for j = 1:2
    plot(score(x1(g(j,:),i),1),score(x1(g(j,:),i),2),cLabel{c},'MarkerSize',12,'LineWidth',1,'MarkerFaceColor',cLabel{c}(1));
    hold on 
    c = c+1;
    end
end

temp1 = x1(1:2,4:6); 
temp2 = x1(3:4,4:6); 
plot(score(temp1(:),1),score(temp1(:),2),cLabel{7},'MarkerSize',12,'LineWidth',1,'MarkerFaceColor',cLabel{7}(1));
plot(score(temp2(:),1),score(temp2(:),2),cLabel{8},'MarkerSize',12,'LineWidth',1,'MarkerFaceColor',cLabel{8}(1));

legend({'S-T1','M-T1','S-T2','M-T2','S-T3','M-T3','S-T(4-6)','M-T(4-6)'})
xlabel('PC1');
ylabel('PC2'); 
set(gca,'FontSize',18)

funPrintImage(h,'Paper_Figure/Figure_1_B');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Change = []; 
for i = 1:4
    Change = [Change oExplorerAggregated.Data{i}.DataPrimitive.Table(:,2) - oExplorerAggregated.Data{i}.DataPrimitive.Table(:,1)];
end

[a b] = sort(var(Change,[],2),'descend');
objclust = clustergram(Change(b(1:9000),:),'Standardize',2);
plot(objclust);

set(0,'ShowHiddenHandles','on');

figureHandle = gcf; 
set(findall(figureHandle,'type','text'),'fontSize',22,'fontWeight','bold');