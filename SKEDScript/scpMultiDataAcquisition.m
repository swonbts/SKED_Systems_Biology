clear 
addpath(['..' filesep 'Data']); %addpath('../Data/');
addpath(['..' filesep 'SKEDCLass']); %addpath('../SKEDClass/'); 
addpath(['..' filesep 'SKEDFunction']); %addpath('../SKEDFunction/');

cExperiment = {'E03','E04','E23','E24','E25'};
for l = 1:length(cExperiment)
    sExpName = cExperiment{l}; %%%Experiment of Interest is E04 
    sFileName = ['..' filesep 'Data' filesep sExpName '.mat'];  %%%%Define Save Destination and File Name 
                    setdbprefs('DataReturnFormat','table')

    if exist(sFileName,'file')  %%%Check if data already Downloaded, otherwise proceed to data aquisition.
        load(sFileName);
    else
        oExperiment = clsSKEDExperiment(sExpName); %%%Construct MDBExperiment object. 
        save(sFileName, 'oExperiment');
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%Data Aquisition 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    sFileName = ['../Data/' sExpName 'CompleteData.mat'];%%%%Define Save Destination and File Name 
    cDataLables = {}; 
    if exist(sFileName,'file') %%%Check if data already Downloaded, otherwise proceed to data aquisition.
        load(sFileName) 
    else 
        %%%%%%%%%% Get INNATE IMMUNE DATA%%%%%%%%%%%%%%%%%
        oExplorer = clsSKEDAnalysis();  %%%Construct oExplorer as MDBAnalysis Object 
        for i =1:length(oExperiment.Subject) %%%For loop to get MDB data object for each Subject
                x = oExperiment.Subject{i};  %%%X is the MDBSubject object of a subejct
                cDataTypes = x.DataTypes;
                c = 1;
                if strcmp('No Data',x.DataTypes{1}) == 0

                    for k = 1:length(cDataTypes)
                        for j = 1:size(x.Data(cDataTypes{k}).DataAssociations,1)
                            temp = x.Data(cDataTypes{k}).DataAssociations{j,1};
                                cMetaData = x.Data(cDataTypes{k}).DataAssociations(j,:);
                                T = x.Data(cDataTypes{k}).getLowFreqTimeSeries(cMetaData);
                                oExplorer.Data{i,c} = T; %%%Append to oExplorer.Data
                                c = c + 1; 
                        end
                    end
                end
        end
        save(sFileName,'oExplorer');
    end
end
